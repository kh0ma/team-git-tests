package com.softserve.edu.git_tests;

import com.softserve.edu.git_tests.message.Message;
import com.softserve.edu.git_tests.message.MessageFactory;
import com.softserve.edu.git_tests.message.MockMessageFactory;

import java.io.BufferedInputStream;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Choose what you want to say... (from 1 to 6)");
        MessageFactory messageFactory = new MockMessageFactory();
        BufferedInputStream inputStream = new BufferedInputStream(System.in);
        Scanner reader = new Scanner(inputStream);
        int messageId = reader.nextInt();
        Message message = messageFactory.getMessage(messageId);
        message.say();
        System.out.println("Blah Blah Blah");
        System.out.println("Shutting down..");
    }
}
