package com.softserve.edu.git_tests.message;

/**
 * @author Olexander Khomenko
 */

public interface MessageFactory {
    Message getMessage(int id);
}
