package com.softserve.edu.git_tests.message;

/**
 * @author Olexander Khomenko
 */

public class MockMessageFactory implements MessageFactory {
    @Override
    public Message getMessage(int id) {
        return new Message() {
            @Override
            public void say() {
                System.out.println("Hello from Mock Message Factory");
            }
        };
    }
}
