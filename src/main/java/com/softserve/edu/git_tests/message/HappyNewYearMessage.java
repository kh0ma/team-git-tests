package com.softserve.edu.git_tests.message;

/**
 * @author Olexander Khomenko
 */

public class HappyNewYearMessage implements Message {
    @Override
    public void say() {
        System.out.println("Happy New Year!");
    }
}
