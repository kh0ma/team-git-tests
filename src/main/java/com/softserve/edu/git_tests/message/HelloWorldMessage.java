package com.softserve.edu.git_tests.message;

/**
 * Created by alin- on 09.01.2018.
 */
public class HelloWorldMessage implements Message {
    @Override
    public void say() {
        System.out.println("Hello World!");
    }
}
