package com.softserve.edu.git_tests.message;

/**
 * @author Olexander Khomenko
 */

public interface Message {
    void say();
}
